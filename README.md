# Ertsen Electro

De ertsen electro is een interactive waarbij het publiek gevraagd wordt een combinatie tussen een metaal en het bijhorende erts te maken.

De combinatie wordt gemaakt door zowel een erts als een metaal aan te raken.

Verschillende metalen van een motorfiets zijn van elkaar (elektrisch) geïsoleerd en via kabels verbonden met de hardware. bij dit spel gebruiken we 5 verschillende metalen/Ertsen

IJzer, koper, chroom, lood en aluminium.

De meting van de combinaties gebeurt middels capacitieve koppeling. De afzonderlijk metalen onderdelen worden om de beurt gedurende 1 milliseconde van een 12 volt potentiaal voorzien.(puls) Als een persoon een metaal aanraakt zal zijn/haar lichaam het elektrisch potentiaal van dat metaal volgen. Als deze persoon ook nog een erts aanraakt dan kunnen de potentiaal sprongetjes met behulp van een elektrode die aan de achterkant van het erts is aangebracht worden waargenomen. Het erts geleidt de stroom niet. Dit werkingsprincipe wordt capacitieve koppeling genoemd.

Onderstaand programma genereert de pulsen voor de metalen onderdelen. De hele kleine signaaltjes die via de ertsen worden opgevangen worden gefilterd en versterkt en worden als interrupt door de processor ontvangen.

Doordat de software "weet" welk metaal een puls heeft ontvangen en kan "zien" van welk erts een signaal terugkomt kan er een goed/fout feedback voor de gebruiker gegenereerd worden.

De 1ms pulsen worden met een serieel-parallel schuifregister (TPIC6A595DW) gegenereerd. Deze chip is via de SPI bus met de processor verbonden.

De feedback voor de gebruiker bestaat uit een groen of rood lichtsignaal en een goed of fout geluidje.

De lichtsignalen worden met WS2812 (smart)leds gemaakt. Hiervoor is de Adafruit FastLED.h library gebruikt.

De geluiden worden met een MP3 module van DFRobot gegenereerd. Hiervoor is de library DFRobotDFPlayerMini.h gebruikt.
