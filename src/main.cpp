// ========================================================================
// === Ertsen Electro
// === De ertsen electro is een interactive waarbij het publiek gevraagd wordt een combinatie tussen een metaal en het bijhorende erts te maken.
// === De combinatie wordt gemaakt door zowel een erts als een metaal aan te raken.
// === Verschillende metalen van een motorfiets zijn van elkaar (elektrisch) geïsoleerd en via kabels
// === verbonden met de hardware. bij dit spel gebruiken we 5 verschillende metalen/Ertsen
// === Ijzer, koper, chroom, lood en aluminium.
// === De meting van de combinaties gebeurt middels capacitieve koppeling.
// === De afzonderlijk metalen onderdelen worden om de beurt gedurende 1 milliseconde van een 12 volt
// === potentiaal voorzien.(puls) Als een persoon een metaal aanraakt zal zijn/haar lichaam het elektrisch potentiaal
// === van dat metaal volgen. Als deze persoon ook nog een erts aanraakt dan kunnen de potentiaal sprongetjes met behulp van
// === een elektrode die aan de achterkant van het erts is aangebracht worden waargenomen. Het erts geleidt de stroom niet.
// === Dit werkingsprincipe wordt capacitieve koppeling genoemd.
// ===
// === Onderstaand programma genereert de pulsen voor de metalen onderdelen. De hele kleine signaaltjes die via de ertsen
// === worden opgevangen worden gefilterd en versterkt en worden als interrupt door de processor ontvangen.
// ===
// === Doordat de software "weet" welk metaal een puls heeft ontvangen en kan "zien" van welk erts een signaal terugkomt kan er 
// ==- een goed/fout feedback voor de gebruiker gegenereerd worden.
// ===
// === De 1ms pulsen worden met een serieel-parallel schuifregister (TPIC6A595DW) gegenereerd. Deze chip is via de SPI bus
// === met de processor verbonden.
// === De feedback voor de gebruiker bestaat uit een groen of rood lichtsignaal en een goed of fout geluidje.
// === De lichtsignalen worden met WS2812 (smart)leds gemaakt. Hiervoor is de Adafruit FastLED.h library gebruikt.
// === De geluiden worden met een MP3 module van DFRobot gegenereerd. Hiervoor is de library DFRobotDFPlayerMini.h gebruikt.
// ===


#include <Arduino.h>



//==========================================================================
// Includes
//==========================================================================

#include <TimerOne.h>
#include "SWTimer.h"
#include <avr/interrupt.h>
#include <SPI.h>

#include <SoftwareSerial.h>
#include "FastLED.h"
#include "DFRobotDFPlayerMini.h" // MP3 player
/*
  

*/
//==========================================================================
//Defines
//==========================================================================

// ====== SW Timers ===========

#define ClrLeds 1
#define PulsInh 2
#define IdleLightTmr 3
#define LedStepTimer 4


#define Inp0 A0
#define Inp1 A1
#define Inp2 A2
#define Inp3 A3
#define Inp4 6


#define TestPuls 10
boolean TP = false;

#define MP3Rx 5
#define MP3Tx 4
#define TIMRx 7
#define TIMTx 13

#define Intr_0 2


#define MaxNrSmpl 15



// Pixelleds
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS 12
#define PixelLedDataOut  3
#define MaxBrightness 64
#define IdleTime 5000

// Aantal timers dat je wilt gebruiken
#define NrTimers 10

#define maxledlevel 8000
#define minledlevel 100

//==========================================================================
// Memory declarations
//==========================================================================


int RCK = 8; // Pin RCK stuurt de output sectie van het shiftregister.


CRGB leds[NUM_LEDS];
uint8_t Brightness = MaxBrightness;
uint8_t MyBrightness = MaxBrightness;
CRGB NewColor;
int LedSteps = 0;

int InpArr[5] {Inp0,Inp1,Inp2,Inp3,Inp4};

volatile boolean ISRFlag = false;
volatile byte CPins = 0;
volatile byte PrefPins = 0;
volatile int CurActive = 0; // variable bevat id van de active output
volatile int NrSmpl[5]; // Array wordt gebruikt om erts <-> onderdeel combinatie te valideren
volatile boolean PulsInhib = false;
boolean FlipFlop;

volatile int Col = 0;
volatile int PTimeout = 0;
volatile int Teller = 0;
volatile int PrTeller = 0;


int ledlevel = maxledlevel - 10;

//=================MP3 player ==============
DFRobotDFPlayerMini myDFPlayer;
int AudioVolume = 29;


// Zet de timers in een array
SWTimer MTimer[NrTimers];

volatile int PulseState = 0;
volatile int Chnl = 0;

SoftwareSerial Mp3Serial(MP3Rx, MP3Tx); // RX, TX



void SetFadeTimers(void)
{
  Brightness = MaxBrightness;
  MTimer[LedStepTimer].Halt(); 
  MTimer[IdleLightTmr].Set(IdleTime);
}
 

void LedsGreen()
{
  Brightness = MaxBrightness;
  NewColor = CRGB::Green;
  LedSteps = 1;
  MTimer[LedStepTimer].Set(50);
  MTimer[IdleLightTmr].Set(IdleTime);
}


void ShowLeds(CRGB MyColor,int Steps)
{
int i;
  for(i=0;i<Steps;i++)
  {
    leds[i] = MyColor;
  }
  FastLED.show();
}

void LedsRed()
{
  Brightness = MaxBrightness;
  NewColor = CRGB::Red;
  LedSteps = 1;
  MTimer[LedStepTimer].Set(50);
  MTimer[IdleLightTmr].Set(IdleTime);

}

void LedsBlue()
{
  Brightness = MaxBrightness;
  NewColor = CRGB::Blue;
  LedSteps = 1;
  MTimer[LedStepTimer].Set(50);
  MTimer[IdleLightTmr].Set(IdleTime);

}

void LedsPink()
{
//  NewColor = 1674448;
  Brightness = 16;
  NewColor = CRGB::BlueViolet;
  LedSteps = 1;
  MTimer[LedStepTimer].Set(50);

//  leds[i] = 16711935; //CRGB::Pink;
//  leds[i] = 1674448; //CRGB::
}


void LedsOff()
{
  NewColor = CRGB::Black;
  LedSteps = 1;
  MTimer[LedStepTimer].Set(50);
}



boolean ReadInp(int n) // Deze functie leest de toestand van input pin n en retourneerd deze als boolean
{
//  digitalWrite(TestPuls,TP=!TP);

  return (digitalRead(InpArr[n]) == LOW);
}




void Intr0R() // Interrupt 0 routine (Pulse detected)
{
ISRFlag = true;
int i;  
  for(i=0;i<5;i++)
  {
    if (ReadInp(i) == true)
    { 
      Teller++;
      if(CurActive != -1)
      {
          digitalWrite(TestPuls,true);
          digitalWrite(TestPuls,false);

        PTimeout = 0;
        if(i == CurActive)
        {
          NrSmpl[CurActive]++;     
        }  
        else
        {
          NrSmpl[CurActive]--;

        }  
      }   
    }  
  }  
}    



void WriteOut(uint8_t Chnl, boolean B) 
{
byte OutByte = 0;
  if (B)
    OutByte = (1 << Chnl);    // bit 0 tm 4 in OutByte wordt gezet
  SPI.transfer(~OutByte);
  digitalWrite(RCK,LOW);
  digitalWrite(RCK,HIGH); 
}

void  ClearOKSampl()// Functie om NrSmpl array te resetten
{
int i;  
  for(i=0;i<5;i++)
  {
    NrSmpl[i] = 0;
  }
}
  
void TimerIntr(void) // Interrupt routine. Wordt 1000 x per seconde uitgevoerd
{

  int i;
  for (i=0; i<NrTimers; i++)
  MTimer[i].Update(); //


  if(PTimeout < 25)
    PTimeout++;
  if(PTimeout == 20)
    ClearOKSampl();

// De pulsen voor de motorfietsonderdelen worden in het volgende switchstatement gecreerd 
 if (!PulsInhib)  
    switch (PulseState)
    {
      case 0:
          CurActive = -1;
          WriteOut(Chnl, true); // Hoog niveau op output n
          PulseState++;
          break;
      case 1:
          CurActive = Chnl;
          WriteOut(Chnl, false); // Output niveau laag (Steile flank)
          PulseState++;
          break;
      case 2:
          CurActive = -1;
          Chnl++;
          if(Chnl > 4) 
            Chnl = 0;
          PulseState = 0;
          break;
    default:
          PulseState = 0;
          break;
    }
    
    

}

// ==================================================================================================
//   S E T U P   
// ==================================================================================================


void setup(void)
{
  int i;
  pinMode(TestPuls, OUTPUT);
  pinMode(RCK,OUTPUT);
  digitalWrite(RCK,HIGH);
  Serial.begin(115200);
  delay(500);
  Serial.println("Hallo 1");
  Mp3Serial.begin(9600); //Serial1 is connected to MP3 player
//  TIMSerial.begin(9600); //Serial1 is connected to TIM Output
  delay(10);
  Serial.println("Hallo 2");
  delay(10);



  pinMode(Inp0, INPUT_PULLUP);
  pinMode(Inp1, INPUT_PULLUP);
  pinMode(Inp2, INPUT_PULLUP);
  pinMode(Inp3, INPUT_PULLUP);
  pinMode(Inp4, INPUT_PULLUP);


  pinMode(Intr_0, INPUT_PULLUP);
  PulsInhib = true;
  Timer1.initialize(1000);  // 1 milisec
  Timer1.attachInterrupt(TimerIntr);
  
  attachInterrupt(digitalPinToInterrupt(Intr_0), Intr0R, RISING);

  FastLED.addLeds<LED_TYPE,PixelLedDataOut,COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(Brightness);

   if (!myDFPlayer.begin(Mp3Serial,false,false)) 
   {  
      Serial.println(F("Unable to begin:"));
   } 

  
  SPI.begin();
  SPI.setDataMode(SPI_MODE0); 
  SPI.setBitOrder(MSBFIRST);
  SPI.setClockDivider(SPI_CLOCK_DIV128);
 

  
  for (i = 0; i < NrTimers; i++)
    MTimer[i].Set(500);
 LedsGreen();
 Serial.println("Einde Setup");
 PulsInhib = false;
}




void Do_Timers(void)
{

  if ( MTimer[ClrLeds].Expired())
  {
    MTimer[ClrLeds].Halt();
       LedsOff();
  }
  if ( MTimer[PulsInh].Expired())
  {
      MTimer[PulsInh].Halt();
      PulsInhib = false;
  }

  if(MTimer[IdleLightTmr].Expired()) // Als deze timer afloopt start de fade-up van de led's (Zie LedStepTimer )
  {
      MTimer[IdleLightTmr].Halt();
      LedsPink();
      Serial.println("Idle");
  }

  if(MTimer[LedStepTimer].Expired())
  {
    Serial.print(".");
    ShowLeds(NewColor,LedSteps++);
    
    if(LedSteps < 9)
      MTimer[LedStepTimer].Set(50); // 50ms 
    else
      MTimer[LedStepTimer].Halt();
  }
}


// =================================================================================================
//== MP3 player
// =================================================================================================

void PlaySound(int n)
{
//  myDFPlayer.volume(AudioVolume);  //Set volume value. From 0 to 30

  myDFPlayer.play(n);
  Serial.println("Geluid");
}

// =====   End of MP3 player   =====================================================================



int CheckConnec(void) // check of er een erts en motoronderdeel wordt aangeraakt
{
int i;

   for (i=0;i<5;i++)
   {
     if(NrSmpl[i] > 4)
     {
       MTimer[ClrLeds].Set(2500);
       LedsGreen();
       PlaySound(1);
//       TIMSerial.println("Hallo");
       PulsInhib = true;
       Serial.println("OK!");
       MTimer[PulsInh].Set(500);
     }  
     if(NrSmpl[i] < -4)
     {
       MTimer[ClrLeds].Set(2500);
       LedsRed();
       PlaySound(2);
       PulsInhib = true;
       Serial.println("Fout!");
       MTimer[PulsInh].Set(500);
     }  
   }
} 



void ShowInp()
{
int i;  
    for(i=0;i<5;i++)
      Serial.print(ReadInp(i));
    Serial.println();  
}

void loop(void)
{
int i;
   Do_Timers();
    
 
    CheckConnec();
    delay(1);
    
  if(PrTeller != Teller)
  {
    PrTeller = Teller;
    for(i=0;i<5;i++)
    {
      Serial.print(NrSmpl[i]);
      Serial.print(" ");
    }
    Serial.println(Teller);
    ShowInp();
  }
  
}