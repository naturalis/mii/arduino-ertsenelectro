#include <TimerOne.h>
#include "SWTimer.h"

/*
 * Hi Jan, 
 * Dit is dan wel geen statemachine maar een timer waar je heel makkelijk getimede acties mee kunt uitvoeren
 * De library SWTimer heb ik zelf gemaakt en ik gebruik hem regelmatig
 * De werking is heel simpel.
 * De timer bestaat uit een teller.
 * Met het update commando verlaag je de waarde van de teller. Het update comando kan het best vanuit een hardware timer interrupt aangeroepen worden
 * Als de teller nul wordt dan wordt de Expired vlag true.
 * De teller geef je een waarde met het Set commando.
 * De timer kan je stoppen met het Halt commando
 * Je kan net zoveel timers gebruiken als je nodig hebt. 
 * Een soort van multitasker eigenlijk
 * 
 * Het voorbeeld hieronder laat zien hoe je automatisch een aantal taken kan laten uitvoeren.
 * Als je een led aansluit op pin 9 dan zie je een langzame fade-up fade-down
 * 
 */

const int led = 9;  // the pin with a LED


// Aantal timers dat je wilt gebruiken
#define NrTimers 10

#define maxledlevel 8000
#define minledlevel 100

int ledlevel = maxledlevel -10;


// Zet de timers in een array
SWTimer MTimer[NrTimers];



void TimersUpd(void) // Interrupt routine. Wordt 100 x per seconde uitgevoerd
{
int i;
  for (i=0; i<NrTimers; i++)
  {
    MTimer[i].Update(); // De counter van de timer wordt met 1 verlaagd. Als ie 0 is dan is ie "Expired"  
  }
}



void setup(void)
{
int i;
  Timer1.initialize(10000);  // 10 milisec
  Timer1.attachInterrupt(TimersUpd); 
  Serial.begin(115200);
  
  //pinMode(led,OUTPUT);
  
  for (i=0; i<NrTimers; i++)
    MTimer[i].Set(50);
    
    MTimer[2].Set(500);
    MTimer[3].Set(600);

}






void Do_Timers(void)
{
int i;
  if ( MTimer[0].Expired()){
    Serial.println("Timer 0: Controleer de tijd");
    MTimer[0].Set(6000); // 1 keer per minuut
 
  }

  if ( MTimer[1].Expired()){
    Serial.println("Timer 1: Controleer accuspanning");
    MTimer[1].Set(1000); // Om de 10 sec
  }


 if( MTimer[2].Expired())
  {
    MTimer[2].Set(500);
    Serial.println("Timer 2: Doe dit...");
  }  


  if( MTimer[3].Expired())
  {
    MTimer[3].Set(500);
    Serial.println("Timer 3: Doe dat...");
  }  

  if( MTimer[8].Expired()) // Timer 8 zorgt voor de fade-up van de led
  {
    MTimer[8].Set(1);
    MTimer[9].Halt(); // Als timer 8 actief ie dan mag timer 9 dat niet zijn
    analogWrite(led,ledlevel+=5);
    if (ledlevel > maxledlevel)
    {
      MTimer[9].Set(5); // Fade-down timer wordt gestart
      MTimer[8].Halt(); // Timer zet zichzelf uit!
    }
  }  
if( MTimer[9].Expired()) // Timer 9 zorgt voor de fade-down van de led
  {
    MTimer[9].Set(1);
    MTimer[8].Halt(); // Als timer 9 actief ie dan mag timer 8 dat niet zijn
    analogWrite(led,ledlevel-=5);
    if (ledlevel < minledlevel)
    {
      MTimer[8].Set(1); // Fade-up timer wordt gestart
      MTimer[9].Halt(); // Timer zet zichzelf uit!
    }
  }  

  
}

void loop(void)
{
// Serial.println(MTimer[3].Get());
  Do_Timers();
  delay(5);
}
